import {Entity, PrimaryGeneratedColumn, Column, AfterUpdate, BeforeUpdate} from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    age: number;

    @Column({ nullable: true })
    updatedAt: Date;   

    @BeforeUpdate()
    public setUpdateDate() {
        this.updatedAt = new Date()
        console.log('updated!')
    }
}
