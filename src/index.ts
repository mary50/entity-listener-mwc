import "reflect-metadata";
import {createConnection} from "typeorm";
import {User} from "./entity/User";

createConnection().then(async connection => {

    console.log("Inserting a new user into the database...");
    const userRepo = connection.getRepository(User)
    

    const user = userRepo.create({
        firstName: "John",
        lastName: "Doe",
        age: 25
    })
    
    user.age = Math.floor(Math.random() * 100)
    await userRepo.save(user)
    console.log(user)

    // const updated =  userRepo.findOne(1)
    // console.log(updated)await

}).catch(error => console.log(error));
